<section class="support">
    <div class="container">
        <div class="row">
            <p class="s-title"><a href="<?php echo get_home_url(); ?>"><img src="<?=get_template_directory_uri()?>/img/xpartner_logo.png" alt=""></a></p>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 sup-column">
                <div class="support-item">
                    <p class="service-name"><a href="">Xerox kundeservice</a></p>
                    <p>Ved teknisk feil eller behov for service og forbruksartikler (toner/trommel) på ditt Xerox utstyr:</p>
                    <a href="skype:<?php echo get_option('support_phone'); ?>?call" class="button tlf">Tlf: <?php echo get_option('support_phone'); ?></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 sup-column">
                <div class="support-item">
                    <p class="service-name"><a href="">Ekstra service</a></p>
                    <p>Vi kan tilby assistanse i forbindelse med installasjon, flytting og teknisk hjelp utenom vanlig service.</p>
                    <a href="mailto:<?php echo get_option('support_email'); ?>" class="button mail"><?php echo get_option('support_email'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>