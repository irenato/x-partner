$(document).ready(function(){
	$('.header-menu i').click(function(){
		$('.main-menu').toggleClass('visible-menu');
	});
});

jQuery(function($){
    $(document).mouseup(function (e){
        var div = $(".col-menu");
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('.main-menu').removeClass('visible-menu'); // скрываем его
        }
    });
});

/*slider*/
$('#mn-slides').superslides({
    animation: 'fade',
    play: 3000,
    pagination:false,
    animation_speed: 'slow',
    animation_easing: 'linear'
});

/*Service call panel*/
$(document).ready(function(){
	$('.btn_call').click(function(){
		$(this).addClass('active_btn');
	});
});
jQuery(function($){
    $(document).mouseup(function (e){
        var div = $(".btn_call"); 
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('.btn_call').removeClass('active_btn'); // скрываем его
        }
    });
});

$(document).ready(function(){
    $('input.contact-form-x-partner').click(function(e) {
        e.preventDefault();
        var name, phone, email, error_message;
        name = $('input.name-x-partner').val();
        phone = $('input.phone-x-partner').val();
        email = $('input.email-x-partner').val();

        if (name == '' || phone == '' || email == '') {
            error_message = 'Fyll ut alle feltene';
            $("div.message-alert p").text(error_message);
            $("div.message-alert").show();
        } else {
            var send_data = $.ajax({
                url: '/wp/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'contact_us',
                    'name': name,
                    'phone': phone,
                    'email': email
                },
            });
            send_data.done(function(action){

                if(action == 'done!'){
                    $('input.name-x-partner').val('');
                    $('input.phone-x-partner').val('');
                    $('input.email-x-partner').val('');
                }else{
                    error_message = 'Meldingen ble ikke sendt';
                    $("div.message-alert p").text(error_message);
                    $("div.message-alert").show();
                }

            })
        }
    })

    $("input.send-button").click(function(e){
        e.preventDefault();
        var name, email, message, error_message;
        name = $("input#name").val();
        email = $("input#email").val();
        message = $("textarea#message").val();

        if (name == '' || message == '' || email == '') {
            error_message = 'Fyll ut alle feltene';
            $("div.mail-form-error-alert p").text(error_message);
            $("div.mail-form-error-alert").show();
        }else{

            var send_data = $.ajax({
                url: '/wp/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'contact_us_support',
                    'name': name,
                    'email': email,
                    'message': message
                },
            });
            send_data.done(function(action){

                if(action == 'done!'){
                    $("input#name").val('');
                    $("input#email").val('');
                    $("textarea#message").val('');
                }else{
                    error_message = 'Meldingen ble ikke sendt';
                    $("div.mail-form-error-alert p").text(error_message);
                    $("div.mail-form-error-alert").show();
                }

            })

        }
    })

    $("a.modal-button").click(function(){
        //alert('1231');
        var youtube_link_for_iframe = $("i.youtube-link-for-iframe").attr('data_src');
        $("div.modal-body iframe").attr('src', youtube_link_for_iframe);
    })

    $("div#videoModal").on('hidden.bs.modal', function () {

        $("div.modal-body iframe").attr('src', '');

    });
})


