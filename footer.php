<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 foot-column">
                <h4>KONTAKT</h4>
                <p>X-Partner Bergen AS <br><b>TIf: <?php echo get_option('phone_number'); ?></b></p>
                <p><?php echo get_option('address_part_1'); ?> <br> <?php echo get_option('address_part_2'); ?></p>
                <a href="">Se kart</a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 foot-column">
                <h4>SOSIALT</h4>
                <ul class="social">
                    <li><a href="<?php echo get_option('facebook_url'); ?>" class="icon-fb"></a></li>
                    <li><a href="<?php echo get_option('linkedin_url'); ?>" class="icon-in"></a></li>
                    <li><a href="<?php echo get_option('instagram_url'); ?>" class="icon-instagram"></a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 foot-column">
                <h4>X-PARTNER AS</h4>
                <p>X-Partner Bergen AS er blant landets største leverandører av Xerox teknologi.</p>
                <a href="http://xpartner.no/bestilling"><b>Bestill rekvisita</b></a>
                
            </div>
        </div>
    </div>
</footer>

<!--Red line under footer section-->
<section class="f-baner">
    <p>XPartner AS - 2016</p>
</section>

<?php wp_footer(); ?>

</body>

</html>