<?php
get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <!--START slider section-->
    <!--        if exists masterslider shortcode for this page use masterslider. else use featured image -->
    <?php sliderSection($post->ID, 'masterslider_shortcode'); ?>
    <!--END slider section-->


    <section class="st-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="content-title"><?php the_title(); ?></h3>
                    <p class="content-subtitle"><?php echo get_field('content-subtitle',$post->ID); ?></p>
                    <p class="content-text"><?php echo get_the_content(); ?> </p>
                    <div class="youtube-wrapper">
                        <?php if(get_field('youtube_link',$post->ID) != '') : ?>
                            <a class="modal-button" data-toggle="modal" data-target="#videoModal"><img src="<?=get_template_directory_uri()?>/img/video.png" alt=""></a>
                        <?php endif; ?>
                        <!-- Modal -->
                        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <?php $youtube_url = str_replace('/watch?v=', '/embed/', get_field('youtube_link',$post->ID)); ?>
                                        <i class="youtube-link-for-iframe" data_src = "<?php echo $youtube_url; ?>"></i>
                                        <iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="content-text">
                            <?php echo get_field('2nd_part_of_text',$post->ID); ?>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </section>

<?php endwhile;?>
<?php endif;?>

    <section class="tjenester">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="tj-title">Andre produkter</p>
                    <div class="t-container">
                        <?php $related_posts = get_posts( array(
                            'numberposts'     => 12, // тоже самое что posts_per_page
                            'offset'          => 0,
                            'post_type' => 'products',
//                                                    'posts_per_page' => -1,
                            'orderby' => 'ID',
                            'order' => 'ASC',
//                                                    'cat'        => '3',
//                                                    'orderby'         => 'meta_value',
//                                                    'order'           => 'DESC',
//                                                    'meta_key'        => 'views',
//                                                    'meta_value'      => '',
//                                                    'post_type'       => 'post',
//                                                    'post_mime_type'  => '', // image, video, video/mp4
//                                                    'post_status'     => 'publish'
                        ) );
                        foreach($related_posts as $related_post) : ?>
                        <!--                        var_dump($post);-->
                        <a href="<?php the_permalink(); ?>"> <div class="tj-item">
                                <p class="tj-icon"><i></i><a href=""><?= $related_post->post_title ?></a></p>
                                <p><?= get_the_excerpt( $related_post );?></p></a>
                        <a href="<?= $related_post->guid ?>" class="more">LES MER</a>
                    </div>
                    <?php endforeach;?>

                </div>
            </div>
        </div>
        </div>
    </section>

<?php
get_template_part( 'support_item');
?>

<?php get_footer(); ?>