<?php
/*
Template Name: About us
*/
get_header();
?>
<?php $args = array('category_name' => 'om oss'); ?>

<?php $post_page_about_us = new WP_query($args);?>

<?php while ( $post_page_about_us->have_posts() ) : $post_page_about_us->the_post(); ?>
<section class="fullw-baner">
    <?php $short_code =  wp_specialchars_decode(get_field('masterslider_shortcode_for_post', $post->ID), ENT_QUOTES);
            echo do_shortcode($short_code); ?>
</section>

<!--Standart content section-->
<section class="st-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="content-title"><?php the_title(); ?></h3>
                <p class="content-subtitle"><?php echo get_field('content-subtitle',$post->ID); ?></p>
                <p class="content-text"><?php echo get_the_content(); ?></p>

                <!--Link for modal yotube window-->
                <div class="youtube-wrapper">
                    <?php if(get_field('youtube_link',$post->ID) != '') : ?>
                        <a class="modal-button" data-toggle="modal" data-target="#videoModal"><img src="<?=get_template_directory_uri()?>/img/video.png" alt=""></a>
                    <?php endif; ?>
                    <!-- Modal -->
                    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <?php $youtube_video_about_us = str_replace('/watch?v=', '/embed/', get_field('youtube_link',$post->ID)); ?>
                                    <i class="youtube-link-for-iframe" data_src = "<?php echo $youtube_video_about_us; ?>"></i>
                                    <iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="content-text">
                        <?php echo get_field('2nd_part_of_text',$post->ID); ?>
                    </p>
                </div>
                <!--END link for modal youtube window-->
            </div>
        </div>
    </div>
</section>

<?php
    get_template_part( 'support_item');
?>

<!--Map section-->
<section class="location">
    <div class="map">
        <?= do_shortcode('[pw_map address="'. get_option('google_maps_street').','.get_option('google_maps_address').', '.get_option('google_maps_city').'" width="100%" height="410px"]'); ?>
<!--        <iframe src="https://www.google.com/maps/embed?key=AIzaSyAaNg2p5Pu3R3wc8dpA7T6Ax-8ejHOTG_Y&q=&q=Butchart+Gardens+Victoria+BC" width="100%" height="410" frameborder="0" style="border:0" allowfullscreen></iframe>-->
<!--        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1658.385958491904!2d5.332867518863721!3d60.37644436164647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m3!3e2!4m0!4m0!5e0!3m2!1sen!2sua!4v1460722371808" width="100%" height="410" frameborder="0" style="border:0" allowfullscreen></iframe>-->
    </div>
</section>
<?php endwhile; ?>
<?php get_footer(); ?>
