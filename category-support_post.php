<?php

get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <!--START slider section-->
    <!--        if exists masterslider shortcode for this page use masterslider. else use featured image -->
    <?php sliderSection($post->ID, 'masterslider_shortcode_for_post'); ?>
    <!--END slider section-->

<!--<section class="blockquote">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-12">-->
<!--               <p></p>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--</section>-->

<section class="st-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="content-title"><?php the_title(); ?></h3>
                <p class="content-subtitle"><?php echo get_field('content-subtitle',$post->ID); ?></p>
                <p class="content-text"><?php echo get_the_content(); ?></p>

                <p class="content-text"><?php echo get_field('2nd_part_of_text',$post->ID); ?></p>
                <div class="youtube-wrapper">
                    <?php if(get_field('youtube_link',$post->ID) != '') : ?>
                        <a class="modal-button" data-toggle="modal" data-target="#videoModal"><img src="<?=get_template_directory_uri()?>/img/video.png" alt=""></a>
                    <?php endif; ?>
                    <!-- Modal -->
                    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <?php $youtube_url = str_replace('/watch?v=', '/embed/', get_field('youtube_link',$post->ID)); ?>
                                    <i class="youtube-link-for-iframe" data_src = "<?php echo $youtube_url; ?>"></i>
                                    <iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<?php endwhile;?>
<?php endif;?>
<?php
    get_template_part( 'support_item');
?>

<?php get_footer(); ?>
