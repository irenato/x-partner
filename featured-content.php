<?php $args = array(
    'numberposts'     => 12, // тоже самое что posts_per_page
    'offset'          => 0,
    'post_type' => 'products',
    'orderby' => 'ID',
    'order' => 'ASC',
    'posts_per_page' => 6); ?>

<?php $post_home_page = new WP_query($args);?>

<?php while ( $post_home_page->have_posts() ) : $post_home_page->the_post(); ?>

    <a href="<?php the_permalink(); ?>"><div class="tj-item">
        <p class="tj-icon"><i></i><?php the_title(); ?></p>
        <p><?= wp_trim_words( get_field('text_1',$post->ID), $num_words = 25, '...' ) ?></p></a>
        <a href="<?php the_permalink(); ?>" class="more">LES MER</a>
    </div>

<?php endwhile; ?>