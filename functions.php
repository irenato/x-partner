<?php


function x_partner_style(){
    wp_enqueue_style( 'x_partner-bootstrap', get_template_directory_uri().'/css/bootstrap.min.css','','', 'all' );
    wp_enqueue_style( 'x_partner-superslides', get_template_directory_uri().'/css/superslides.css','','', 'all' );
    wp_enqueue_style( 'x_partner-screen', get_template_directory_uri() .'/css/screen.css','','', 'all' );
    wp_enqueue_style( 'x_partner-ico', get_template_directory_uri() .'/css/xpartner.css','','', 'all' );
    wp_enqueue_style( 'x_partner-print', get_template_directory_uri() .'/css/print.css','','', 'all' );
    wp_enqueue_style( 'x_partner-ie', get_template_directory_uri() .'/css/ie.css','','', 'all' );
}
add_action( 'wp_enqueue_scripts', 'x_partner_style' );

function x_partner_scripts(){
//    wp_enqueue_script( 'x_partner-jquery-1.12.3.min', get_template_directory_uri().'/js/jquery-1.12.3.min.js',false, '', true);
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script( 'x_partner-bootstrap.min', get_template_directory_uri().'/js/bootstrap.min.js',false, '', true);
    wp_enqueue_script( 'x_partner-jquery.superslides', get_template_directory_uri().'/js/jquery.superslides.js',false, '', true);
//    wp_enqueue_script( 'x_partner-jquery.superslides.min', get_template_directory_uri().'/js/jquery.superslides.min.js',false, '', true);
    wp_enqueue_script( 'x_partner-jscript', get_template_directory_uri().'/js/jscript.js',false, '', true);
    wp_localize_script( 'x_partner-login', 'ajax_object', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}

add_action( 'wp_enqueue_scripts', 'x_partner_scripts' );

add_theme_support( 'post-thumbnails' );

if ( ! function_exists( 'x_partner_theme_setup' ) ) :

    function x_partner_theme_setup() {

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
            'top_main' => __( 'Top Menu'),
        ));

        function my_wp_nav_menu_args( $args='' ){
            $args['container'] = '';

            return $args;
        }
        add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );


    }
endif;
add_action( 'after_setup_theme', 'x_partner_theme_setup' );

function x_partner_full_menu_setup($menu_name='top_main'){
    $options = get_option( 'theme_settings' );
    $menu_list="";
    if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
        global $post;
        $page_for_posts = get_option( 'page_for_posts' );

        if($page_for_posts){
            if(is_home()) {
                $thePostID= $page_for_posts ;
            }
            else{
                $thePostID = $post->ID;
            }
        }
        else{
            $thePostID = $post->ID;
        }

        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list = '<ul>';
        $submenu = false;
        $parent_id=0;
        $count=0;
        $i = -1;

        foreach ((array) $menu_items as $menu_item) {
            $title = $menu_item->title;
            $url = $menu_item->url;
            $active_class=$thePostID==$menu_item->object_id? "active":"";

            if ( !$menu_item->menu_item_parent ){

                $menu_list.=' <li class="' . $active_class . '"><a href="' . $url . '">' . $title . '</a></li>';

            }

            $count++;
        }
        $menu_list .= "</ul>";
    }
    return $menu_list;
}

function contact_us(){
    $name = stripcslashes(trim($_POST['name']));
    $phone = stripcslashes(trim($_POST['phone']));
    $email = stripcslashes(trim($_POST['email']));
    $to = get_option('company_email');
    $headers = 'From: '.$name .'<'.$email.'>' . "\r\n";
    $mail_subject = 'Brukeren '.$name.' ønsker å kontakte deg';
    $mail_text = 'Brukeren '.$name.' ønsker å kontakte deg.
    telefonnummer: '.$phone;

    if($mail_send = wp_mail($to, $mail_subject, $mail_text, $headers)){
        print('done!');
        die();
    }else{
        print('error!');
        die();
    }

}

add_action('wp_ajax_nopriv_contact_us', 'contact_us');
add_action('wp_ajax_contact_us', 'contact_us');

function contact_us_support(){
    $name = stripcslashes(trim($_POST['name']));
    $email = stripcslashes(trim($_POST['email']));
    $message = stripcslashes(trim($_POST['message']));
    $to = get_option('company_email');
    $headers = 'From: '.$name .'<'.$email.'>' . "\r\n";
    $mail_subject = 'Melding fra brukeren'.$name;
    $mail_text = $message;

    if($mail_send = wp_mail($to, $mail_subject, $mail_text, $headers)){
        print('done!');
        die();
    }else{
        print('error!');
        die();
    }
}

add_action('wp_ajax_nopriv_contact_us_support', 'contact_us_support');
add_action('wp_ajax_contact_us_support', 'contact_us_support');

add_action('wp_head', 'kama_postviews');
function kama_postviews() {

    $meta_key       = 'views';  // Ключ мета поля, куда будет записываться количество просмотров.
    $who_count      = 0;            // Чьи посещения считать? 0 - Всех. 1 - Только гостей. 2 - Только зарегистрированных пользователей.
    $exclude_bots   = 1;            // Исключить ботов, роботов, пауков и прочую нечесть :)? 0 - нет, пусть тоже считаются. 1 - да, исключить из подсчета.

    global $user_ID, $post;
    if(is_singular()) {
        $id = (int)$post->ID;
        static $post_views = false;
        if($post_views) return true; // чтобы 1 раз за поток
        $post_views = (int)get_post_meta($id,$meta_key, true);
        $should_count = false;
        switch( (int)$who_count ) {
            case 0: $should_count = true;
                break;
            case 1:
                if( (int)$user_ID == 0 )
                    $should_count = true;
                break;
            case 2:
                if( (int)$user_ID > 0 )
                    $should_count = true;
                break;
        }
        if( (int)$exclude_bots==1 && $should_count ){
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            $notbot = "Mozilla|Opera"; //Chrome|Safari|Firefox|Netscape - все равны Mozilla
            $bot = "Bot/|robot|Slurp/|yahoo"; //Яндекс иногда как Mozilla представляется
            if ( !preg_match("/$notbot/i", $useragent) || preg_match("!$bot!i", $useragent) )
                $should_count = false;
        }

        if($should_count)
            if( !update_post_meta($id, $meta_key, ($post_views+1)) ) add_post_meta($id, $meta_key, 1, true);
    }
    return true;
}

function themeslug_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 'themeslug_logo_section' , array(
        'title'       => __( 'Logo', 'themeslug' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting( 'themeslug_logo' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
        'label'    => __( 'Logo', 'themeslug' ),
        'section'  => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    ) ) );
}
add_action( 'customize_register', 'themeslug_theme_customizer' );


function sliderSection($post_id, $shortcode_type){

            $master_slider = get_field($shortcode_type, $post_id);
            if (!empty( $master_slider ) ) : ?>
                <section class="fs-slider">
                    <div class="col-lg-12">

                        <?php $short_code = wp_specialchars_decode($master_slider, ENT_QUOTES); ?>
                        <?= do_shortcode($short_code); ?>

                    </div>
                </section>
            <?php else : ?>
                <section class="fullw-baner">
                    <div class="img-container" style="background: url(<?= get_the_post_thumbnail_url( $post_id, 'full' ) ?>)"></div>
                </section>
            <?php endif;
}

add_action('init', 'sections_register');
function sections_register(){
    register_post_type('products',
        array(
            'label' => __('Produkter'),
            'singular_label' => 'produkt',
            'public' => true,
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => array('title', 'thumbnail'
            ),
        )
    );

//    register_taxonomy('products-category',
//        'products',
//        array('hierarchical' => true,
//            'label' => __('Kategori'),
//            'sort' => true,
//            'args' => array('orderby' => 'term_order'),
//            'rewrite' => true
//        )
//    );
}

