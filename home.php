<?php
/*
Template Name: Home
*/
get_header();
?>
<?php $home = get_page_by_title( 'Home' ); ?>

<!--START slider section-->
<!--        if exists masterslider shortcode for this page use masterslider. else use featured image -->
    <?php sliderSection(5, 'masterslider_shortcode'); ?>
<!--END slider section-->

<!--Posts link section-->
<section class="tjenester">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="tj-title">
<!--                    --><?php
//                        global $wp;
//                        $current_url = home_url(add_query_arg(array(),$wp->request));
//                        var_dump(get_the_ID()) ?>
<!--                    --><?php //die(); ?>

                    <?= get_field('title', 5) ?> </p>
                <div class="t-container">

                    <?php
                        get_template_part( 'featured-content');
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!--END posts link section-->

<!--Support section-->
<?php
    get_template_part( 'support_item');
?>
<?php get_footer(); ?>
