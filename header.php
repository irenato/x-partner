<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= wp_get_document_title('|', true, 'right');?></title>
    <?php wp_head(); ?>

</head>
<body>


<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                <a href="<?php echo get_home_url(); ?>" class="logo">
                    <img src="<?=get_template_directory_uri()?>/img/xpartner_logo.png" alt="">
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
                <p>Sertifisert Xerox Gold Partner</p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 col-menu pull-right">
                <span class="header-menu">MENY<i class="icon-interface"></i></span>
            </div>
            <div class="col-lg-4 pull-right">
                <div class="btn_call strong">
                    <div class="span">
                        <div class="btn_info">
                            <p>KUNDESERVICE</p>
                            <a href="">T <?php echo get_option('customer_phone'); ?></a>
                            <p class="descr-btn">Solheimsgaten 15, 5058 Bergen</p>
                            <form action="">
                                <input class="name-x-partner" type="text" placeholder="Navn">
                                <input class="phone-x-partner" type="text" placeholder="Telefon">
                                <input class="email-x-partner" type="email" placeholder="Epost">
                                <div class="message-alert" style="display: none">
                                    <p></p>
                                </div>
                                <input class="contact-form-x-partner" type="submit" value="Kontakt oss">
                            </form>
                        </div>
                    </div>
                    <div class="shake-icon small">
                        <img src="<?=get_template_directory_uri()?>/img/btn_call.png" alt="">
                        <p>Kontakt oss</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-menu">
        <?php echo x_partner_full_menu_setup('top_main'); ?>
<!--        <ul>-->
<!--            <li><a href="http://xpartner.alscon-clients.com/OmOss.html">Om oss</a></li>-->
<!--            <li><a href="http://xpartner.alscon-clients.com/support.html">support</a></li>-->
<!--            <li><a href="http://xpartner.alscon-clients.com/quotes.html">quotes</a></li>-->
<!--        </ul>-->
    </div>
</header>