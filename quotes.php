<?php
/*
Template Name: Quotes
*/
get_header();
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <!--START slider section-->
    <!--        if exists masterslider shortcode for this page use masterslider. else use featured image -->
        <?php sliderSection($post->ID, 'masterslider_shortcode'); ?>
    <!--END slider section-->


<section class="blockquote">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <span class="bl-img"></span>
                    <p><?php echo get_the_content(); ?> </p>
                <?php endwhile;?>
                <?php endif;?>
            </div>
        </div>
    </div>

</section>

<section class="st-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="content-subtitle"><?= get_field('subtitle_of_quote', $post->ID) ?></p>
                <p class="content-text"><?= get_field('description_of_quote', $post->ID) ?></p>

                <p class="content-text"><?= get_field('description_of_quote_part_2', $post->ID) ?></p>

            </div>
        </div>
    </div>
</section>

<?php
    get_template_part( 'support_item');
?>

<?php get_footer(); ?>
