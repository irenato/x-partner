<?php
/*
Template Name: Support
*/
get_header();
?>


<!--START slider section-->
<!--        if exists masterslider shortcode for this page use masterslider. else use featured image -->
    <?php sliderSection(7, 'masterslider_shortcode'); ?>
<!--END slider section-->

<section class="st-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <h3 class="content-title"><?php the_title(); ?></h3>
                    <p class="content-text"><?php echo get_the_content(); ?></p>
                <?php endwhile;?>
                <?php endif;?>
                <div class="boxes">
                <?php $args = array('category_name' => 'support_post',
                    'posts_per_page' => 6); ?>
                <?php $post_support_page = new WP_query($args);?>
                <?php while ( $post_support_page->have_posts() ) : $post_support_page->the_post(); ?>
                    <div class="box-item">
                        <a href="<?php the_permalink(); ?>">
                            <div class="icon-cont"></div>
                        </a>
                        <div class="title-cont">
                            <p class="b-heading"><?php the_title(); ?></p>
                            <p><?php the_excerpt(); ?></p>
                            <a href="<?php the_permalink(); ?>">Les mer her</a>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>

            </div>
        </div>
    </div>
</section>

<?php
    get_template_part( 'support_item');
?>

<section class="contact-form">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="">
                        <p>Lurer du på noe?</p>
						<span>
							<label for="name">Navn</label>
							<input type="text" id="name">
						</span>
						<span>
							<label for="email">E-post</label>
							<input type="text" id="email">
						</span>
                        <label for="message">Melding</label>
                        <textarea name="message" id="message"></textarea>
                        <div class="mail-form-error-alert" style="display: none">
                            <p></p>
                        </div>
                        <input type="submit" value="SEND" class="send-button">


                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
